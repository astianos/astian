# Astian OS #

### Submitting Patches ###

Patches are always welcome!

Fork our repo's, make your changes, commit with a detailed message and send us a pull request.

# Getting Started #

Requirements for building Astian OS:

* Running Archlinux System (64-bit)
* archiso package
* astiankiso package

Building a Astian OS live media is pretty easy.

Clone our "astian os" profile from https://bitbucket.org/astianos/astian-os/ to your local machine, change into it's dir and start the build by running:

sudo ./build.sh -v

If you need more infos about archiso and how it works, visit the official archlinux wiki: https://wiki.archlinux.org/index.php/Archiso